import supertest from 'supertest';
import { urls } from '../config/index';

const MailboxRequests = function MailboxRequests() {
    this.validateEmail = async function validateEmail(email) {
        const response = await supertest(urls.mailboxWithKey)
            .post(`&email=${email}`)
            .set('Accept', 'application/json');
        return response;
    };

    this.validateEmailWithoutKey = async function validateEmailWithoutKey(email) {
        const response = await supertest(urls.mailboxWithoutKey)
            .post(`&email=${email}`)
            .set('Accept', 'application/json');
        return response;
    };

    this.emailScoreFunction = function emailScoreFunction(emailScore) {
        let result;
        if (emailScore <= 1.00 && emailScore >= 0.65) {
            result = 'good';
        } else if (emailScore <= 0.64 && emailScore >= 0.33) {
            result = 'medium';
        } else if (emailScore <= 0.48 && emailScore >= 0.00) {
            result = 'bad';
        }
        return result;
    };
};

export { MailboxRequests };
