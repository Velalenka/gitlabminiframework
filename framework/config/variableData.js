
const variableData = {
    mailboxValidEmail: 'testuser@gmail.com',
    mailboxInvalidEmail: 'testuser',
    mailboxSupportEmail: 'support@apilayer.com',
};
export { variableData };
