import { MailboxRequests } from './services/index';

const ApiProvider = () => ({
    info: () => new MailboxRequests(),
});

export { ApiProvider };
