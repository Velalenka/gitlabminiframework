import { describe, expect, test } from '@jest/globals';
import { variableData } from '../framework/config/index';
import { ApiProvider } from '../framework';

describe('Validating emails', () => {
    test('Service returning 200', async () => {
        const response = await ApiProvider().info().validateEmail(variableData.mailboxValidEmail);
        expect(response.status).toEqual(200);
    });

    test('Email is valid', async () => {
        const response = await ApiProvider().info().validateEmail(variableData.mailboxValidEmail);
        expect(response.body.format_valid).toEqual(true);
    });

    test('Email without domen', async () => {
        const response = await ApiProvider().info().validateEmail(variableData.mailboxInvalidEmail);
        expect(response.body.domain).toEqual(null);
    });

    test('No email supplied', async () => {
        const response = await ApiProvider().info().validateEmail('');
        expect(response.body.error.type).toEqual('no_email_address_supplied');
    });

    test('Email with free provider', async () => {
        const response = await ApiProvider().info().validateEmail(variableData.mailboxValidEmail);
        expect(response.body.free).toEqual(true);
    });

    test('Support email', async () => {
        const response = await ApiProvider().info().validateEmail(variableData.mailboxSupportEmail);
        expect(response.body.role).toEqual(true);
    });

    test('Email score', async () => {
        const response = await ApiProvider().info().validateEmail(variableData.mailboxValidEmail);
        const emailScoreResult = ApiProvider().info().emailScoreFunction(response.body.score);
        expect(emailScoreResult).toEqual('medium');
    });

    test('No api key supplied', async () => {
        const response = await ApiProvider().info()
            .validateEmailWithoutKey(variableData.mailboxValidEmail);
        expect(response.body.error.type).toEqual('missing_access_key');
    });
});
